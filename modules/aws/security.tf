resource "aws_security_group" "allow_outbound_all" {
  name        = "allow_outbound_all"
  description = "Allow all outbound traffic"
  vpc_id      = "${aws_vpc.vpc-1.id}"

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.label}"
  }
}

resource "aws_security_group" "allow_inbound_all" {
  name        = "allow_inbound_all"
  description = "Allow all inbound traffic"
  vpc_id      = "${aws_vpc.vpc-1.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.label}"
  }
}

resource "aws_security_group" "allow_inbound_ssh" {
  name        = "allow_inbound_ssh"
  description = "Allow inbound ssh traffic"
  vpc_id      = "${aws_vpc.vpc-1.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.label}"
  }
}

resource "aws_security_group" "allow_inbound_web" {
  name        = "allow_inbound_web"
  description = "Allow inbound web traffic"
  vpc_id      = "${aws_vpc.vpc-1.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.label}"
  }
}

resource "aws_key_pair" "pubkey-1" {
  key_name   = "public-key-1"
  public_key = "${var.pubkey}"
}

