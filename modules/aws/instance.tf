data "aws_ami" "ubuntu1804" {
  owners = ["099720109477"] # Canonical
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_ami" "centos7" {
  owners      = ["679593333241"]
  most_recent = true
  filter {
    name   = "name"
    values = ["CentOS Linux 7 x86_64 HVM EBS *"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

resource "aws_instance" "sshinstance" {
  ami                         = "${data.aws_ami.ubuntu1804.id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${aws_subnet.subnet-public-1.id}"
  vpc_security_group_ids      = [ "${aws_security_group.allow_outbound_all.id}",
                                  "${aws_security_group.allow_inbound_ssh.id}" ]
  associate_public_ip_address = "true"
  key_name                    = "${aws_key_pair.pubkey-1.id}"

  tags {
    Name = "${var.label}"
  }
}

data "template_file" "userdata_web" {
  template = "${file("${path.module}/templates/userdata_web.tpl")}"
}

resource "aws_instance" "webinstance" {
  ami                         = "${data.aws_ami.centos7.id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${aws_subnet.subnet-public-1.id}"
  vpc_security_group_ids      = [ "${aws_security_group.allow_outbound_all.id}",
                                  "${aws_security_group.allow_inbound_ssh.id}",
                                  "${aws_security_group.allow_inbound_web.id}" ]
  associate_public_ip_address = "true"
  key_name                    = "${aws_key_pair.pubkey-1.id}"
  user_data                   = "${data.template_file.userdata_web.rendered}"

  tags {
    Name = "${var.label}"
  }
}
