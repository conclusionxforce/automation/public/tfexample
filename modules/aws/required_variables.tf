# See AWS -> IAM -> Users -> Username -> Security Credentials -> Access keys
variable "aws_access_key" {
  type    = "string"
  default = "<Access key from AWS>"
  # Example: default = "SJIAJRDJWOHIDDBPEEDC"
}

# See AWS -> IAM -> Users -> Username -> Security Credentials -> Access keys
variable "aws_secret_key" {
  type    = "string"
  default = "<Secret part of the AWS access key>"
  # Example: default = "y+Dmmas9sajasdjaSJJKLj2988HbhjkhkEJ99JS3"
}

# Create a keypair with "ssh-keygen -t rsa -f ~/.ssh/awskey" if you don't have one
variable "pubkey" {
  type    = "string"
  default = "<Contents of your public key (~/.ssh/awskey.pub)>"
  # Example: default = "ssh-rsa AAAAB3NzaCadslsdsaSLKJnBAAABAQCi+U9qCKJJJN987KHJsnbnmoSKJHSb87JKHMd0VNju6Dv1l04Qz2E2Jbks4ipcMZXucxX0jPHA0ttgen2NtNc56TpqiJohSHZ4LtayP47wo56dvuKiszXip3h769rW8rbqHK7JGKkfXCodW3sO32FJF+XxCD7JgHE63NOWQrl4X+3EoifhKG4vZXlP4LOChOE/YP9XJT8dFEfX0FvC75v/IIw/B3qvT1dDnzF7KQuGq6t3mK9XV9Jl/z3xBqI8YW/2EVVk4fEgDh1gK6KwdEbpdylQq2t0SmZGVVZoDwxk0QIFUNIzvSrobmBgexCVlS8rS/jztlPuqjTquHVkOGDP user@hostname"
}
