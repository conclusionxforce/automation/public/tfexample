output "ssh_pubip" {
  value = "${aws_instance.sshinstance.public_ip}"
}

output "web_pubip" {
  value = "${aws_instance.webinstance.public_ip}"
}
