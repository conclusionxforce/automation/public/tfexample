variable "label" {
  type    = "string"
  default = "mylabel"
}

variable "awsregion" {
  type    = "string"
  default = "eu-west-1"
}

variable "vpc_cidr" {
  type    = "string"
  default = "172.101.0.0/16"
}

variable "subnet_pub" {
  type    = "string"
  default = "172.101.1.0/24"
}

variable "ntpserver1" {
  type    = "string"
  # time.google.com=216.239.35.8
  default = "216.239.25.8"
}
