resource "aws_vpc" "vpc-1" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags {
    Name = "${var.label}"
  }
}

resource "aws_subnet" "subnet-public-1" {
  vpc_id                  = "${aws_vpc.vpc-1.id}"
  cidr_block              = "${var.subnet_pub}"
  availability_zone       = "${var.awsregion}a"
  map_public_ip_on_launch = true

  tags {
    Name = "${var.label}"
  }
}

resource "aws_vpc_dhcp_options" "dhcp-options-1" {
  domain_name_servers = ["AmazonProvidedDNS"]
  domain_name         = "${var.awsregion}.compute.internal"
  ntp_servers         = ["${var.ntpserver1}"]

  tags {
    Name = "${var.label}"
  }
}

resource "aws_vpc_dhcp_options_association" "dhcp-options-association-1" {
  vpc_id          = "${aws_vpc.vpc-1.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.dhcp-options-1.id}"
}

resource "aws_internet_gateway" "gw-1" {
  vpc_id = "${aws_vpc.vpc-1.id}"

  tags {
    Name = "${var.label}"
  }
}

resource "aws_route" "route-1" {
  route_table_id         = "${aws_vpc.vpc-1.default_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gw-1.id}"
}

resource "aws_route_table_association" "route-subnet-public-1" {
  subnet_id      = "${aws_subnet.subnet-public-1.id}"
  route_table_id = "${aws_vpc.vpc-1.default_route_table_id}"
}

