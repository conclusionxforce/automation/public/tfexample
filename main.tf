module "aws" {
  source  = "modules/aws"
}

output "Public IP address Ubuntu 18.04 ssh instance (user: ubuntu)" {                                    
  value = "${module.aws.ssh_pubip}"
}

output "Public IP address CentOS 7 web instance (user: centos)" {
  value = "${module.aws.web_pubip}"
}

