Jacco Logtenberg (2018-11-10)

E: jacco.logtenberg@conclusionxforce.nl

W: https://conclusionxforce.nl

## Download this code
`git clone https://gitlab.com/conclusionxforce/automation/public/tfexample.git`

## What does this code do?
* Create a VPC
  * with a subnet for public instances (with a public IP-address and internet communication)
  * with an internet gateway
  * routing to/from the subnet/gateway
  * with a DHCP server for the subnet
* Create the following security groups
  * "*allow_outbound_all*"
  * "*allow_inbound_all*"
  * "*allow_inbound_ssh*" - 22/TCP
  * "*allow_inbound_web*" - 80/TCP and 443/TCP
* Create an Ubuntu 18.04 instance which is accessible with ssh (TCP/22) on a public IP address
* Create a CentOS 7 instance which is accessible with ssh (TCP/22) and a browser (http, TCP/80) on a public IP address
* Configure the instances to be accessed by a private key (uploads the corresponding public key to AWS)

Everything is created with a custom label, so you can easily identify your resources in AWS and it doesn't conflict with existing resources.

The AWS-account you use must have sufficient permissions to create a VPC and other networking stuff.

## What doesn't it do?
* Install terraform
  * Download it from https://www.terraform.io/downloads.html
  * Unzip the downloaded file to a location in your $PATH (for example: /usr/local/bin)
* Create a ssh private/public keypair
  * You can use a previously defined keypair
  * Or create a fresh one with `ssh-keygen -t rsa -f ~/.ssh/awskey`

## How to use this code?
* Edit the file `modules/aws/required_variables.tf` and modify the following variables:
  * `aws_access_key` - the API key used for accessing the AWS API.
  * `aws_secret_key` - the "password" of that API key. 
  * `pubkey` - contents of the public key to use when logging in to the instances. It will be placed in the `~/.ssh/authorized_keys` on the instance.
* Optionally edit the file `modules/aws/optional_variables.tf` with the following variables:
  * `label` - your personal identification string (default: mylabel)
  * `awsregion` - the AWS region you want to use (default: eu-west-1, which is Ireland)
  * `vpc_cidr` - the IP-subnet for your VPC. This should be an unused CIDR for that region (default: 172.101.0.0/16)
  * `subnet_pub` - the IP-subnet (part of the VPC subnet) which is used for internet-facing instances (default: 172.101.1.0/24)
* Run `terraform init`. This will initialize Terraform and download the configured provider plugins.
* Run `terraform plan` to check if the terraform configuration is compatible with the online AWS situation.
* Run `terraform apply` to create all the stuff in AWS.

When succesfully applied, the public IP adresses of the instances are displayed:
```
$ terraform apply

Apply complete! Resources: 14 added, 0 changed, 0 destroyed.

Outputs:
Public IP address CentOS 7 web instance (user: centos) = 10.20.30.40
Public IP address Ubuntu 18.04 ssh instance (user: ubuntu) = 11.21.31.41
```

Now you know the public IP addresses, you can login with ssh to the instances using your private key:
```
$ ssh -i ~/.ssh/awskey centos@10.20.30.40
...
[centos@ip-172-101-1-120 ~]$ 
```

## How to remove the created configuration from AWS
* Run `terraform destroy`

